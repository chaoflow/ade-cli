# -*- coding: utf-8 -*-
#
# Copyright 2016 - 2018  Ternaris.
# SPDX-License-Identifier: Apache-2.0

"""Agile Development Environment"""

__version__ = '3.5.0dev'
