.. _changelog:

Changelog
---------

This project follows `SemVer <https://semver.org/>`_.


.. _upcoming_changes:

Upcoming (unreleased)
^^^^^^^^^^^^^^^^^^^^^

Added
~~~~~
- Add initial set of documentation
- Extend ``ade start --help`` to include information on publishing ports

Changed
~~~~~~~

Deprecated
~~~~~~~~~~

Removed
~~~~~~~

Fixed
~~~~~

Security
~~~~~~~~


.. _v3.4.1:

3.4.1 (2018-11-16)
^^^^^^^^^^^^^^^^^^

Changed
~~~~~~~
- Pinned aiohttp<3.5.0 for compatibility with Python 3.5.2


.. _v3.4.0:

3.4.0 (2018-11-16)
^^^^^^^^^^^^^^^^^^

Changed
~~~~~~~
- Print n/a in image matrix for images that have not been pulled


.. _v3.3.1:

3.3.1 (2018-11-15)
^^^^^^^^^^^^^^^^^^

Added
~~~~~
- Add environment variables to introspect loaded image versions
- Print git commit or tag info in image matrix
- Add CI job for release validation and publication

.. _v3.2.0:

3.2.0 (2018-11-14)
^^^^^^^^^^^^^^^^^^

Added
~~~~~
- Add ``--version`` option
- Add nvidia-docker support
- Add workaround for broken ``docker exec``
- Add tests and initial CI jobs
- Add ADE_CLI_VERSION environment variables
- Add infrastructure to add requirements for development on ade-cli

Changed
~~~~~~~
- Renamed ``--check`` to ``--update``
- Pin to release Click 7.0
- Code cleanup


.. _v3.1.0:

3.1.0 (2018-10-10)
^^^^^^^^^^^^^^^^^^

Added
~~~~~
- Support setting docker run args via configuration

Fixed
~~~~~
- Fix passing docker run arguments to ade start


.. _v3.0.1:

3.0.1 (2018-09-11)
^^^^^^^^^^^^^^^^^^

Fixed
~~~~~
- Fix homepage and project URLs


.. _v3.0.0:

3.0.0 (2018-09-04)
^^^^^^^^^^^^^^^^^^

Added
~~~~~
- Initial public release of ADE
