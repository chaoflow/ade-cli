.. _aderc-file:

The ``.aderc`` File
^^^^^^^^^^^^^^^^^^^

As described in :ref:`ade-env-vars`, it is possible to configure ADE using environment
variables. In order to make these configurations project-wide, add the environment
variables to the ``.aderc``. At a minimum, the ``.aderc`` file should include the list
of images

.. literalinclude:: minimal-ade/.aderc
   :language: bash

Often, it will also need to include the Gitlab instance and registry, so ``ade`` knows
where to download images:

.. literalinclude:: example-aderc
   :language: bash
   :lines: 6-12

Lastly, it will also need to include some extra arguments to ``docker run`` to enable
debugging, for example:

.. literalinclude:: example-aderc
   :language: bash
   :lines: 1-2, 5-12

See :ref:`addargs` for more information.
