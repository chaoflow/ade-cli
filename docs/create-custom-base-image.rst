.. _create-custom-base-image:

Creating a custom base image
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 2
   :hidden:

   entrypoint-file


The `Dockerfile`_ in the `minimal-ade project`_ shows a minimal example of how to create
a custom base image:

   .. literalinclude:: minimal-ade/Dockerfile
      :language: docker

Let's walk through it:

1. An existing Docker image

   .. literalinclude:: minimal-ade/Dockerfile
      :language: docker
      :lines: 1

2. Some convenience programs and requirements of the ``entrypoint``:

   .. literalinclude:: minimal-ade/Dockerfile
      :language: docker
      :lines: 3-17


3. env.sh_:

   .. literalinclude:: minimal-ade/Dockerfile
      :language: docker
      :lines: 19

   * Configures the default environment on ``ade enter``
   * Source the ``.env.sh`` files provided by volumes (see :ref:`create-custom-volume`)

4. entrypoint_ :

   .. literalinclude:: minimal-ade/Dockerfile
      :language: docker
      :lines: 20-21

   See also :ref:`entrypoint-file`.

5. ``CMD``:

   .. literalinclude:: minimal-ade/Dockerfile
      :language: docker
      :lines: 22

   * This command attaches a process to PID 1 which will keep the container running even if no one
     is attached to the container


.. _Dockerfile: https://gitlab.com/ApexAI/minimal-ade/blob/master/Dockerfile
.. _minimal-ade project: https://gitlab.com/ApexAI/minimal-ade
.. _env.sh: https://gitlab.com/ApexAI/minimal-ade/blob/master/env.sh
.. _entrypoint: https://gitlab.com/ApexAI/minimal-ade/blob/master/entrypoint
