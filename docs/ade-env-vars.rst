.. _ade-env-vars:

Configuring ADE with environment variables
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

For every ``ade`` command's arguments, there is a corresponding environment
variable that can be used to configure each argument's default behavior.
The environment variables follow the following pattern
``ADE_[ADE_COMMAND]_[OPTION_OR_ARGUMENT]``.

In addition to the command-specific variables, the ``ADE_NAME`` can be used
to change the name of the ADE instance. This variable is especially useful
when :ref:`start-multiple-ade` at once.

For example, the default behavior of ``ade start`` is just to start the container,
but often, it is desired to also enter the container directly after starting,
so ``ade start`` has the ``--enter`` option::

  $ ade start --enter
  ade$ # Inside ADE prompt

It is possible to set the default behavior of ``ade start`` by setting the
``ADE_START_ENTER`` variable::

  $ export ADE_START_ENTER=true
  $ ade start
  ade$ # Inside ADE prompt

To make the configuration permanent, add the ``export`` to:

* The ``~/.bashrc`` file on the host, for personal configurations
* The ``.aderc`` file on the project, for project-wide configuration, see
  :ref:`aderc-file`
