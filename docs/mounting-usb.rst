.. _mounting-usb:

Mounting USB devices in ADE
^^^^^^^^^^^^^^^^^^^^^^^^^^^

To mount USB devices to ADE, they must be connected to the computer **before starting ADE**.
Unfortunately, this requirements means that reconnecting a device requires a restart of ADE.
USB devices are mounted by passing `native Docker commands`_ to ``ade start``. e.g.::

   $ ade start -- --device /dev/bus/usb:/dev/bus/usb

As described in :ref:`ade-env-vars` and :ref:`aderc-file`, use the ``ADE_DOCKER_RUN_ARGS``
variable to avoid typing the argument every time.

.. _native Docker commands: https://docs.docker.com/engine/reference/commandline/run/#add-host-device-to-container---device


Troubleshooting USB devices
"""""""""""""""""""""""""""

The device is available when ADE is started, but then it disappears
   Check that the device is connected to a stable power source. Some PCI card and USB hubs
   will not provide constant power, so device will disconnect intermittently

A temporary workaround for USB problems is to mount all USB devcies:
   **Warning**: This is not recommended as a permanent solution because it requires the 
   ``--privileged flag``, which gives Docker nearly all the same access to the host as
   processes running outside containers on the host (see the `Docker documentation`_)::

      $ ade start -- --privileged -v /dev/bus/usb:/dev/bus/usb -v /dev/ttyUSB0:/dev/ttyUSB0

.. _Docker documentation: https://docs.docker.com/engine/reference/run/#runtime-privilege-and-linux-capabilities
