.. _addargs:

Custom ``docker run`` arguments
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ade`` encapsulates most of the options needed to start and run the Docker container
with the correct ``docker run`` options. However, a power user of Docker may find
certain options missing. ``ade start`` allows users to pass additional arguments to
``docker run``.

To pass options, separate them with ``--`` from the preceding ``ade`` options.
For example, use the following command to mount port 1234 into ``ade``::

   ade start -- --publish 127.0.0.1:1234:1234/udp

See https://docs.docker.com/engine/reference/commandline/run/ for more information on ``docker run``.

See :ref:`start-ade-macvlan` and :ref:`mounting-usb` for other examples of ``ADDARGS``.

See :ref:`aderc-file` for ways to make these options permanent for a project.

