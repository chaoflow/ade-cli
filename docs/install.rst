.. _install:

Installation
------------

Requirements
^^^^^^^^^^^^

A Linux distribution with the following programs installed:
   * Docker 18.06 or newer, follow the `instructions for your Linux distribution`_
   * ``python3-pip``
If the host machine has an NVIDIA graphics card, also install:
   * ``nvidia-docker``
   * ``nvidia-modprobe``


Installation
^^^^^^^^^^^^

ADE is published on PyPI. It needs Python >= 3.5.2 and pip. All other
dependencies will be fetched by pip.

::

   sudo apt-get install python3-pip
   pip3 install ade-cli


Autocompletion
^^^^^^^^^^^^^^

To enable autocompletion, add the following to ``~/.bashrc``::

   eval "$(_ADE_COMPLETE=source ade)"

For zsh users add this to your .zshrc::

   eval "$(_ADE_COMPLETE=source-zsh ade)"


See :ref:`usage` for next steps.


.. _instructions for your Linux distribution: https://docs.docker.com/install/#server
